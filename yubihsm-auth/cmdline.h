/** @file cmdline.h
 *  @brief The header file for the command line option parser
 *  generated by GNU Gengetopt version 2.23
 *  http://www.gnu.org/software/gengetopt.
 *  DO NOT modify this file, since it can be overwritten
 *  @author GNU Gengetopt */

#ifndef CMDLINE_H
#define CMDLINE_H

/* If we use autoconf.  */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h> /* for FILE */

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#ifndef CMDLINE_PARSER_PACKAGE
/** @brief the program name (used for printing errors) */
#define CMDLINE_PARSER_PACKAGE PACKAGE
#endif

#ifndef CMDLINE_PARSER_PACKAGE_NAME
/** @brief the complete program name (used for help and version) */
#ifdef PACKAGE_NAME
#define CMDLINE_PARSER_PACKAGE_NAME PACKAGE_NAME
#else
#define CMDLINE_PARSER_PACKAGE_NAME PACKAGE
#endif
#endif

#ifndef CMDLINE_PARSER_VERSION
/** @brief the program version */
#define CMDLINE_PARSER_VERSION VERSION
#endif

enum enum_action { action__NULL = -1, action_arg_calculate = 0, action_arg_changeMINUS_mgmkey, action_arg_delete, action_arg_list, action_arg_put, action_arg_reset, action_arg_retries, action_arg_version, action_arg_getMINUS_challenge, action_arg_getMINUS_pubkey };
enum enum_touch { touch__NULL = -1, touch_arg_off = 0, touch_arg_on };

/** @brief Where the command line options are stored */
struct gengetopt_args_info
{
  const char *help_help; /**< @brief Print help and exit help description.  */
  const char *version_help; /**< @brief Print version and exit help description.  */
  enum enum_action action_arg;	/**< @brief Action to perform.  */
  char * action_orig;	/**< @brief Action to perform original value given at command line.  */
  const char *action_help; /**< @brief Action to perform help description.  */
  char * mgmkey_arg;	/**< @brief Management key is required to put and delete credentials. Sometimes, this key is also referenced as 'Admin Access Code' (default='00000000000000000000000000000000').  */
  char * mgmkey_orig;	/**< @brief Management key is required to put and delete credentials. Sometimes, this key is also referenced as 'Admin Access Code' original value given at command line.  */
  const char *mgmkey_help; /**< @brief Management key is required to put and delete credentials. Sometimes, this key is also referenced as 'Admin Access Code' help description.  */
  char * new_mgmkey_arg;	/**< @brief New management key (default='').  */
  char * new_mgmkey_orig;	/**< @brief New management key original value given at command line.  */
  const char *new_mgmkey_help; /**< @brief New management key help description.  */
  char * credpwd_arg;	/**< @brief Credential password is used to access the credential when logging into the YubiHSM2. Sometimes, this password is also referenced as 'User Access Code' (default='').  */
  char * credpwd_orig;	/**< @brief Credential password is used to access the credential when logging into the YubiHSM2. Sometimes, this password is also referenced as 'User Access Code' original value given at command line.  */
  const char *credpwd_help; /**< @brief Credential password is used to access the credential when logging into the YubiHSM2. Sometimes, this password is also referenced as 'User Access Code' help description.  */
  char * label_arg;	/**< @brief Credential label (default='').  */
  char * label_orig;	/**< @brief Credential label original value given at command line.  */
  const char *label_help; /**< @brief Credential label help description.  */
  char * reader_arg;	/**< @brief Only use a matching reader (default='').  */
  char * reader_orig;	/**< @brief Only use a matching reader original value given at command line.  */
  const char *reader_help; /**< @brief Only use a matching reader help description.  */
  enum enum_touch touch_arg;	/**< @brief Touch required (default='off').  */
  char * touch_orig;	/**< @brief Touch required original value given at command line.  */
  const char *touch_help; /**< @brief Touch required help description.  */
  char * context_arg;	/**< @brief Session keys calculation context (default='').  */
  char * context_orig;	/**< @brief Session keys calculation context original value given at command line.  */
  const char *context_help; /**< @brief Session keys calculation context help description.  */
  int verbose_arg;	/**< @brief Print more information (default='0').  */
  char * verbose_orig;	/**< @brief Print more information original value given at command line.  */
  const char *verbose_help; /**< @brief Print more information help description.  */
  char * derivation_password_arg;	/**< @brief Derivation password for encryption and MAC keys (default='').  */
  char * derivation_password_orig;	/**< @brief Derivation password for encryption and MAC keys original value given at command line.  */
  const char *derivation_password_help; /**< @brief Derivation password for encryption and MAC keys help description.  */
  char * enckey_arg;	/**< @brief Encryption key (default='').  */
  char * enckey_orig;	/**< @brief Encryption key original value given at command line.  */
  const char *enckey_help; /**< @brief Encryption key help description.  */
  char * mackey_arg;	/**< @brief MAC key (default='').  */
  char * mackey_orig;	/**< @brief MAC key original value given at command line.  */
  const char *mackey_help; /**< @brief MAC key help description.  */
  char * privkey_arg;	/**< @brief Private key (default='').  */
  char * privkey_orig;	/**< @brief Private key original value given at command line.  */
  const char *privkey_help; /**< @brief Private key help description.  */
  
  unsigned int help_given ;	/**< @brief Whether help was given.  */
  unsigned int version_given ;	/**< @brief Whether version was given.  */
  unsigned int action_given ;	/**< @brief Whether action was given.  */
  unsigned int mgmkey_given ;	/**< @brief Whether mgmkey was given.  */
  unsigned int new_mgmkey_given ;	/**< @brief Whether new-mgmkey was given.  */
  unsigned int credpwd_given ;	/**< @brief Whether credpwd was given.  */
  unsigned int label_given ;	/**< @brief Whether label was given.  */
  unsigned int reader_given ;	/**< @brief Whether reader was given.  */
  unsigned int touch_given ;	/**< @brief Whether touch was given.  */
  unsigned int context_given ;	/**< @brief Whether context was given.  */
  unsigned int verbose_given ;	/**< @brief Whether verbose was given.  */
  unsigned int derivation_password_given ;	/**< @brief Whether derivation-password was given.  */
  unsigned int enckey_given ;	/**< @brief Whether enckey was given.  */
  unsigned int mackey_given ;	/**< @brief Whether mackey was given.  */
  unsigned int privkey_given ;	/**< @brief Whether privkey was given.  */

  int asymmetric_mode_counter; /**< @brief Counter for mode asymmetric */
  int derivation_mode_counter; /**< @brief Counter for mode derivation */
  int explicit_mode_counter; /**< @brief Counter for mode explicit */
} ;

/** @brief The additional parameters to pass to parser functions */
struct cmdline_parser_params
{
  int override; /**< @brief whether to override possibly already present options (default 0) */
  int initialize; /**< @brief whether to initialize the option structure gengetopt_args_info (default 1) */
  int check_required; /**< @brief whether to check that all required options were provided (default 1) */
  int check_ambiguity; /**< @brief whether to check for options already specified in the option structure gengetopt_args_info (default 0) */
  int print_errors; /**< @brief whether getopt_long should print an error message for a bad option (default 1) */
} ;

/** @brief the purpose string of the program */
extern const char *gengetopt_args_info_purpose;
/** @brief the usage string of the program */
extern const char *gengetopt_args_info_usage;
/** @brief the description string of the program */
extern const char *gengetopt_args_info_description;
/** @brief all the lines making the help output */
extern const char *gengetopt_args_info_help[];

/**
 * The command line parser
 * @param argc the number of command line options
 * @param argv the command line options
 * @param args_info the structure where option information will be stored
 * @return 0 if everything went fine, NON 0 if an error took place
 */
int cmdline_parser (int argc, char **argv,
  struct gengetopt_args_info *args_info);

/**
 * The command line parser (version with additional parameters - deprecated)
 * @param argc the number of command line options
 * @param argv the command line options
 * @param args_info the structure where option information will be stored
 * @param override whether to override possibly already present options
 * @param initialize whether to initialize the option structure my_args_info
 * @param check_required whether to check that all required options were provided
 * @return 0 if everything went fine, NON 0 if an error took place
 * @deprecated use cmdline_parser_ext() instead
 */
int cmdline_parser2 (int argc, char **argv,
  struct gengetopt_args_info *args_info,
  int override, int initialize, int check_required);

/**
 * The command line parser (version with additional parameters)
 * @param argc the number of command line options
 * @param argv the command line options
 * @param args_info the structure where option information will be stored
 * @param params additional parameters for the parser
 * @return 0 if everything went fine, NON 0 if an error took place
 */
int cmdline_parser_ext (int argc, char **argv,
  struct gengetopt_args_info *args_info,
  struct cmdline_parser_params *params);

/**
 * Save the contents of the option struct into an already open FILE stream.
 * @param outfile the stream where to dump options
 * @param args_info the option struct to dump
 * @return 0 if everything went fine, NON 0 if an error took place
 */
int cmdline_parser_dump(FILE *outfile,
  struct gengetopt_args_info *args_info);

/**
 * Save the contents of the option struct into a (text) file.
 * This file can be read by the config file parser (if generated by gengetopt)
 * @param filename the file where to save
 * @param args_info the option struct to save
 * @return 0 if everything went fine, NON 0 if an error took place
 */
int cmdline_parser_file_save(const char *filename,
  struct gengetopt_args_info *args_info);

/**
 * Print the help
 */
void cmdline_parser_print_help(void);
/**
 * Print the version
 */
void cmdline_parser_print_version(void);

/**
 * Initializes all the fields a cmdline_parser_params structure 
 * to their default values
 * @param params the structure to initialize
 */
void cmdline_parser_params_init(struct cmdline_parser_params *params);

/**
 * Allocates dynamically a cmdline_parser_params structure and initializes
 * all its fields to their default values
 * @return the created and initialized cmdline_parser_params structure
 */
struct cmdline_parser_params *cmdline_parser_params_create(void);

/**
 * Initializes the passed gengetopt_args_info structure's fields
 * (also set default values for options that have a default)
 * @param args_info the structure to initialize
 */
void cmdline_parser_init (struct gengetopt_args_info *args_info);
/**
 * Deallocates the string fields of the gengetopt_args_info structure
 * (but does not deallocate the structure itself)
 * @param args_info the structure to deallocate
 */
void cmdline_parser_free (struct gengetopt_args_info *args_info);

/**
 * Checks that all the required options were specified
 * @param args_info the structure to check
 * @param prog_name the name of the program that will be used to print
 *   possible errors
 * @return
 */
int cmdline_parser_required (struct gengetopt_args_info *args_info,
  const char *prog_name);

extern const char *cmdline_parser_action_values[];  /**< @brief Possible values for action. */
extern const char *cmdline_parser_touch_values[];  /**< @brief Possible values for touch. */


#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* CMDLINE_H */
